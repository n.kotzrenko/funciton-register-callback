require('./types')
const mySql = require('mysql')

/**
 * Insert the code into database for future run
 * @param {Environment} env
 * @param {RequestParams} params 
 * @returns {Promise<void>}
 */
exports.insertToDatabase = (env, params) => new Promise((resolve, reject) => {
    const conn = mySql.createConnection({
        user: env.dbUser,
        password: env.dbPassword,
        database: env.dbName,
        socketPath: env.dbSocketPath
    })
    conn.connect(err => {
        if (err) {
            conn.end()
            return reject(err)
        }
            
        conn.query(
            'INSERT INTO funcs(cron_time, callback, params) VALUES(?, ?, ?);',
            [params.cronExpression, params.fnCode, params.params],
            (err, results) => {
                if (err) {
                    conn.end()
                    return reject(err)
                }
                conn.end()
                return resolve(results.insertId)
            }
        )
    })
})