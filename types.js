/**
 * Represents the environment the lambda function runs in
 * @typedef {{
 *  dbHost: string,
 *  dbUser: string,
 *  dbPassword: string,
 *  dbName: string,
 *  dbSocketPath: string,
 *  runnerEndpoint: string,
 *  runnerProjectId: string,
 *  runnerLocationId: string,
 *  runnerServiceId: string,
 *  runnerTimeZone: string,
 * }} Environment 
 */

/**
 * Lambda function request parameters
 * @typedef {{
 *  cronExpression: string,
 *  fnCode: string,
 *  params: object
 * }} RequestParams 
 */