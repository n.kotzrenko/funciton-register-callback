require('./types')
const scheduler = require('@google-cloud/scheduler')

/**
 * Schedule a job to run the callbacks registered under the cron time
 * @param {Environment} env
 * @param {RequestParams} params
 */
exports.scheduleJob = async (env, params) => {
    const client = new scheduler.CloudSchedulerClient()
    const parent = client.locationPath(env.runnerProjectId, env.runnerLocationId)
    const cronJobName = cronTimeToJob(params.cronExpression)
    const job = {
        name: `projects/${env.runnerProjectId}/locations/${env.runnerLocationId}/jobs/${cronJobName}`,
        httpTarget: {
            uri: env.runnerEndpoint,
            httpMethod: 'POST',
            body: Buffer.from(JSON.stringify({ jobname: cronJobName })),
        },
        schedule: params.cronExpression,
        timeZone: env.runnerTimeZone
    }
    const [response] = await client.createJob({
        parent,
        job
    })
    return response
}

/**
 * Format crontime into a valid job name
 * @param {string} cron String representing crontime
 */
const cronTimeToJob = cron => cron.replace(/ /g, '_').replace(/\*/g, '-')