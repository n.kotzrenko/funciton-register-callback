require('./types')
const {translateEnvironmentParams} = require('./env')
const {insertToDatabase} = require('./database')
const {scheduleJob} = require('./schedule')

/**
 * The lambda function itself
 * @param {express.Request} req Request object from the express framework
 * @param {express.response} res Response object from the express framework
 */
exports.registerCallback = async (req, res) => {
    try {
        const env = translateEnvironmentParams()
        const params = extractRequestParams(req)

        console.log('Execution parameters are:')
        console.log(params)
        
        console.log('Begin inserting callback into database')
        const id = await insertToDatabase(env, params)

        console.log('Begin schedualing a trigger to execute the callback')
        await scheduleJob(env, params)
        
        res.status(200).json({ id })
    } catch (err) {
        console.error(err)
        res.sendStatus(500).end()
    }
}

/**
 * Extract parameters from request for ease of usage
 * @param {express.Request} req Request in express framework
 * @returns {RequestParams}
*/
const extractRequestParams = req => {
    return {
        cronExpression: req.body.runAt,
        fnCode: req.body.callbackFn,
        params: req.body.callParameters
    }
}